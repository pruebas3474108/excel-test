<table>
    <tbody>
        <tr>
            <td colspan="4" style="font-weight: bold; text-align: center;">TEST EXCEL</td>
        </tr>
        <tr>
            <td style="font-weight: bold;">Fecha:</td>
            <td colspan="3" style="text-align: center">{{ date("Y-m-d") }}</td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
        <tr>
            <td style="text-align: center;">ID</td>
            <td style="text-align: center;">Nombre</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($municipios as $municipio)
            <tr>
                <td>{{ $municipio->id }}</td>
                <td>{{ $municipio->nombre }}</td>
            </tr>
        @endforeach
    </tbody>
</table>