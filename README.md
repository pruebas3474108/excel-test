# excel-test

## Laravel Excel
Para la generación del excel se uso lo siguiente.
[Documentacion](https://docs.laravel-excel.com/3.1/getting-started/)
## instalación
- Ejecutar el siguiente comando.
```
composer require maatwebsite/excel
```
- Posteriormente debemos registrar Maatwebsite\Excel\ExcelServiceProvider en nuestro config/app.php de la siguiente manera.
```php
'providers' => [
    Maatwebsite\Excel\ExcelServiceProvider::class,
]
```
- De igual forma debemos registrar el facade.
```php
'aliases' => [
    'Excel' => Maatwebsite\Excel\Facades\Excel::class,
]
```
-Por último publicamos la configuracion con el siguiente comando, el cual nos creara una configuracion en config/excel.php
```
php artisan vendor:publish --provider="Maatwebsite\Excel\ExcelServiceProvider" --tag=config
```
## Notas
La base de datos de prueba esta en el proyecto.