<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Municipio;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class MunicipiosExport implements FromView, ShouldAutoSize, WithStyles
{
    /**
     * Constructor
     */
    public function __construct($municipios)
    {
        $this->municipios = $municipios;
    }

    /**
     * Estilos
     */
    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('I')->getAlignment()->setWrapText(true);
    }
    
    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $municipios = $this->municipios;
        // Retornamos la vista en donde se generara el excel
        return view('excels.municipiosExcel', compact('municipios'));
    }
}
