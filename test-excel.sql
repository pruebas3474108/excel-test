-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.33 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para excel-test
CREATE DATABASE IF NOT EXISTS `excel-test` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `excel-test`;

-- Volcando estructura para tabla excel-test.municipios
CREATE TABLE IF NOT EXISTS `municipios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`nombre`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla excel-test.municipios: ~13 rows (aproximadamente)
/*!40000 ALTER TABLE `municipios` DISABLE KEYS */;
INSERT INTO `municipios` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
	(1, 'OTHON P. BLANCO', '2022-11-18 10:17:55', '2022-11-08 13:28:37'),
	(2, 'BACALAR', '2022-11-18 10:17:55', '2022-11-18 10:17:55'),
	(3, 'BENITO JUAREZ', '2022-11-18 10:17:56', '2022-11-18 10:17:56'),
	(4, 'COZUMEL', '2022-11-18 10:17:56', '2022-11-18 10:17:56'),
	(5, 'FELIPE CARRILLO PUERTO', '2022-11-18 10:17:56', '2022-11-18 10:17:56'),
	(6, 'ISLA MUEJERES', '2022-11-18 10:17:56', '2022-11-18 10:17:56'),
	(7, 'JOSE MARIA MORELOS', '2022-11-18 10:17:56', '2022-11-18 10:17:56'),
	(8, 'LAZARO CARDENAS', '2022-11-18 10:17:56', '2022-11-18 10:17:56'),
	(9, 'PUERTO MORELOS', '2022-11-18 10:17:56', '2022-11-18 10:17:56'),
	(10, 'SOLIDARIDAD', '2022-11-18 10:17:56', '2022-11-18 10:17:56'),
	(11, 'TULUM', '2022-11-18 10:17:57', '2022-11-18 10:17:57'),
	(12, 'FUERA DEL ESTADO', '2022-11-18 10:17:57', '2022-11-18 10:17:57'),
	(13, 'MUNICIPIO TEMPORAL', '2022-11-18 10:39:15', '2022-11-18 10:39:15');
/*!40000 ALTER TABLE `municipios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
