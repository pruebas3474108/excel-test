<?php

use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Municipio;
use App\Exports\MunicipiosExport;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/excel', function(){
    // Obtenemos los datos.
    $municipios = Municipio::all();
    // Retornamos el excel para su descarga
    return Excel::download(new MunicipiosExport($municipios), 'municipios.xlsx');
});